#!/usr/bin/env perl

use strict;
use warnings;
use lib "/usr/lib/nagios/plugins";	# Debian
###use lib "/usr/local/nagios/libexec";	# something else
use utils qw($TIMEOUT %ERRORS &print_revision &support &usage);	# Requires utils.pm to be in one of the two directories above
use Getopt::Long qw(:config no_ignore_case);
use File::Basename;

# Initialize variables
my $min = '10';
my $max = '40';
my $warning = '25';
my $critical = '26';
my $sensor_id;
my $help;
my $PROGNAME = basename($0, '.pl');

### functions
sub print_usage ()
{
  print   "Usage:\n",
          "  $PROGNAME [-i|--id=] <SensorID> [-w|--warning=<INTEGER>] [-c|--critical=<INTEGER>] [--min=<INTEGER>] [--max=<INTEGER>]\n",
          "  $PROGNAME [-h|--help]\n",
          "\n";
}

# Long usage info
sub print_help()
{
  print_usage();

  print "Options:\n",
        "  -h, --help\n",
        "     Print this help screen\n",
        "  -i, --id\n",
        "     Sensor ID, see below the listing\n",
        "  -w, --warning=INTEGER\n",
        "     exit with WARNING status, if temperature[°C] is higher\n",
        "  -c, --critical=INTEGER\n",
        "     exit with CRITICAL status, if temperature[°C] is higher\n",
        "  --min=INTEGER\n",
        "     define min temperature[°C] in performance data to prescale graph\n",
        "  --max=INTEGER\n",
        "     define max temperature[°C] in performance data to prescale graph\n",
        "\n",
        "Examples:\n",
        "  $PROGNAME -i 28-00000b10433e\n",
        "  $PROGNAME --id=28-00000b10433e --warning=25 --critical=28 --min=10 --max=30\n",
        "\n",
        "Notes:\n",
        "  This script checks a ONE-WIRE sensor eg. on a Raspberry Pi\n",
        "  https://tutorials-raspberrypi.de/raspberry-pi-temperatur-mittels-sensor-messen/\n",
        "\n",
        "  Listing of sensor directory below:\n",
        "\n",
        `/usr/bin/find /sys/bus/w1/devices/`,
        "\n",
        "-- 8.10.2019 - August Yannikis-Spohn - august\@yannikis.com --\n",
        "\n";
  exit $ERRORS{'OK'};
}
### END OF functions

########################################################################
# get options from command line
my $valid_options = GetOptions (
  "i|id=s"         => \$sensor_id,
  "w|warning=s"	   => \$warning,
  "c|critical=s"   => \$critical,
  "min=s"          => \$min,
  "max=s"          => \$max,
  "help"           => \$help,
);

print_help() unless $sensor_id;
print_help() if $help;
print_help() unless $valid_options;

### Get temperature in m°C from sensor
my $sensor = "/sys/bus/w1/devices/".$sensor_id."/w1_slave";
my $temperature = `/bin/grep 't=' $sensor | /usr/bin/cut -d'=' -f2` / 1000 or die("Getting sensor value FAILED!\n");

### eval status
my $status = 'OK';
if   ($temperature > $critical) { $status = 'CRITICAL'; }
elsif($temperature > $warning)  { $status = 'WARNING'; }

### performance data
my $perfdata = sprintf "Temperature=%2.1f;",$temperature;
$perfdata .= sprintf "%d;",$warning;
$perfdata .= sprintf "%d;",$critical;
$perfdata .= sprintf "%d;",$min;
$perfdata .= sprintf "%d" ,$max;

### print output
printf "%s - Temperature: %2.1f°C | %s\n",$status,$temperature,$perfdata;
exit $ERRORS{$status};
